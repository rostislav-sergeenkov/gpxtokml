<?php

/**
 * @file
 * File gpxtokml.server.inc.
 */

function gpxtokml_server() {
	if (!empty($_POST['label']) && !empty($_POST['coords']) && !empty($_POST['icon'])) {
		$coords = explode(',', $_POST['coords']);

    if (count($coords) == 2) {
      $label = check_plain($_POST['label']);
      $icon = check_plain($_POST['icon']);
      $latitude = check_plain($coords[0]);
      $longitude = check_plain($coords[1]);

      // Saves the data to the database.
      $nid = db_insert('gpxtokml_chrome_extension_data')
        ->fields([
          'label' => $label,
          'icon' => $icon,
          'latitude' => $latitude,
          'longitude' => $longitude,
          'created' => REQUEST_TIME,
        ])
        ->execute();

      if ($nid) {
        return [
          'status' => 'ok',
        ];
      }
    }
	}

  return [
    'status' => 'error',
  ];
} 
 
function gpxtokml_server_form($form, &$form_state) {
  if (!empty(GPXTOKML_SERVER_META_REFRESH_INTERVAL)) {
    $meta_refresh = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'content' =>  GPXTOKML_SERVER_META_REFRESH_INTERVAL,
        'http-equiv' => 'refresh',
      )
    );

    // Add header meta tag for IE to head
    drupal_add_html_head($meta_refresh, 'meta_refresh');
  }

  $form['refresh'] = [
    '#markup' => '<p><strong>' . t('Meta-refresh interval') . '</strong>: ' . GPXTOKML_SERVER_META_REFRESH_INTERVAL . ' sec</p>',
  ];

  $form['truncate'] = [
    '#type' => 'submit',
    '#value' => t('Truncate GPS data'),
    '#attributes' => [
      'style' => 'border: 1px solid red; color: red',
    ],
  ];

  $form['gps'] = [
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  ];

  $form['gps']['gps_points'] = [
    '#markup' => views_embed_view('gpxtokml_gps_coordinates'),
  ];

  return $form;
}

function gpxtokml_server_form_validate($form, &$form_state) {

}

function gpxtokml_server_form_submit($form, &$form_state) {
  if ($form_state['values']['truncate']) {
    db_truncate('gpxtokml_chrome_extension_data')->execute();
    drupal_set_message('GPS data was truncated.');
  }
}
