/**
 * @see http://orda.of.by/.map/?54.48897,27.163408&m=rkka/14,3v/13,e/13,wig/13
 * @see http://getinstance.info/articles/tools/create-chrome-extension-10-minutes-flat
 */
document.addEventListener('DOMContentLoaded', function() {
  var gpsForm = document.getElementById('gps_form');
  var sendButton = document.getElementById('send');
  var coords = document.getElementById('coords');

  chrome.tabs.getSelected(null,function(tab) {
    var parser = document.createElement('a');
    parser.href = tab.url;
    var queryParams = parser.search;
    var params = queryParams.split('&');
    var coordinates = params[0].substring(1);

    if (!coordinates.length) {
      coords.style.borderColor = 'red';
    }
    else {
      coords.value = coordinates;

      // Send GPS data on the fly.
      // sendButton.value = 'GPS data was sent';
	  // sendButton.style.display = 'none';
      // sendButton.style.borderColor = 'green';
      // sendButton.disabled = true;
      // gpsForm.submit();
    }
  });

  // Send GPS data on click.
  sendButton.addEventListener('click', function() {
    this.value = 'Done';
    this.disabled = true;
    gpsForm.submit();
  }, false);
}, false);
