<?php

/**
 * @file
 * File gpxtokml.admin.inc.
 */

/**
 * Admin form.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function gpxtokml_admin_form($form, &$form_state) {
  $form['#tree'] = TRUE;
  $form['#attributes']['enctype'] = 'multipart/form-data';
  
//  $form['switcher'] = [
//    '#type' => 'radios',
//    '#title' => t('Select file processing mode'),
//    '#options' => [
//      'gpxtokml' => t('GPX (Lowrance) -> KML (Google Earth, MAPS.ME)'),
//      'kmltogpx' => t('KML (Google Earth, MAPS.ME) -> GPX (Lowrance)'),
//    ],
//  ];
//
//  $form['gpxtokml'] = [
//    '#type' => 'fieldset',
//    '#collapsible' => FALSE,
//    '#collapsed' => FALSE,
//  ];
//
//  foreach (gpxtokml_get_gpxtokml_icons() as $gpxtokml_icon) {
//    $form['gpxtokml']['icons'][$gpxtokml_icon] = [
//      '#title' => $gpxtokml_icon,
//      '#type' => 'select',
//      '#options' => gpxtokml_get_kmltogpx_icons(),
//    ];
//  }

  $form['gpx_to_kml'] = [
    '#title' => t('LOWRANCE GPX to KML'),
    '#type' => 'fieldset',
	'#collapsible' => FALSE,
	'#collapsed' => FALSE,
  ];
  
  $form['gpx_to_kml']['icons_set'] = [
    '#type' => 'radios',
    '#title' => t('Destination KML Icons Set'),
    '#options' => [
      'mapsme' => t('MAPS.ME points'),
      'lowrance' => t('LOWRANCE icons (for Google Earth)'),
    ],
    '#default_value' => 'lowrance',
  ];  

  $form['gpx_to_kml']['file'] = [
    '#type' => 'file',
    '#title' => t('LOWRANCE GPX file'),
    '#description' => t('Upload a GPX file, allowed extensions: gpx.'),
    '#upload_validators' => [
      'file_validate_extensions' => ['gpx'],
    ],
  ];

  $form['gpx_to_kml']['file_hidden'] = [
    '#type' => 'hidden',
    '#default_value' => '',
  ];

  $form['gpx_to_kml']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Transform GPX to KML'),
    '#validate' => ['gpxtokml_admin_form_validate_transform'],
    '#submit' => ['gpxtokml_admin_form_transform'],
	'#attributes' => [
	  'data-id' => 'gpx_to_kml',
	],	
  ];
  
  $form['kml_to_gpx'] = [
    '#title' => t('KML to LOWRANCE GPX'),
    '#type' => 'fieldset',
	'#collapsible' => FALSE,
	'#collapsed' => FALSE,
  ];  
  
  $form['kml_to_gpx']['icons_set'] = [
    '#type' => 'radios',
    '#title' => t('Source KML Icons Set'),
    '#options' => [
      'mapsme' => t('MAPS.ME points'),
      'lowrance' => t('Google Earth icons'),
    ],
    '#default_value' => 'lowrance',
  ];    
  
  $form['kml_to_gpx']['file'] = [
    '#type' => 'file',
    '#title' => t('MAPS.ME KML file'),
    '#description' => t('Upload a KML file, allowed extensions: kml.'),
    '#upload_validators' => [
      'file_validate_extensions' => ['kml'],
    ],
  ];  
  
  $form['kml_to_gpx']['file_hidden'] = [
    '#type' => 'hidden',
    '#default_value' => '',
  ];    
  
  $form['kml_to_gpx']['submit_kml_to_gpx'] = [
    '#type' => 'submit',
    '#value' => t('Transform KML to GPX'),
    '#validate' => ['gpxtokml_admin_form_validate_transform'],
    '#submit' => ['gpxtokml_admin_form_transform'],
	'#attributes' => [
	  'data-id' => 'kml_to_gpx',
	],
  ];   
  
  $form['db_to_gpx'] = [
    '#title' => t('DATABASE to LOWRANCE GPX'),
    '#type' => 'fieldset',
	'#collapsible' => FALSE,
	'#collapsed' => FALSE,
  ];    

  $form['db_to_gpx']['export_label']	= [
    '#type' => 'select',
    '#title' => t('Select label'),
    '#options' => gpxtokml_admin_form_transform_get_options(),
    '#empty_value' => '',
    '#empty_option' => t('Please select'),
    '#multiple' => TRUE,
    '#attributes' => [
      'style' => 'height: 300px',
    ],
    // '#options' => gpxtokml_admin_form_transform_get_options(),
  ];

  $form['db_to_gpx']['export'] = [
    '#type' => 'submit',
    '#value' => t('Export GPX'),
    // '#validate' => ['gpxtokml_admin_form_validate'],
    '#submit' => ['gpxtokml_admin_form_export'],
	'#attributes' => [
	  'data-id' => 'db_to_gpx',
	],
  ];    

  return $form;
}

/**
 * Validate callback.
 */
function gpxtokml_admin_form_validate_transform($form, &$form_state) {
  $data_id = !empty($form_state['triggering_element']['#attributes']['data-id']) ? 
    $form_state['triggering_element']['#attributes']['data-id'] : '';
	
  if (empty($data_id)) {
    return;
  }	
  
  $file = file_save_upload($data_id, 
    ['file_validate_extensions' => $form[$data_id]['file']['#upload_validators']['file_validate_extensions']]);
  $temp_uri = !empty($file) ? $file->uri : '';

  if ($file) {
    $gpx_dir = GPXTOKML_GPX_FOLDER;
    file_prepare_directory($gpx_dir, FILE_CREATE_DIRECTORY);

    if ($file = file_unmanaged_move($file->uri, $gpx_dir)) {
      $form_state['values']['file'] = $file;
    }
    else {
      form_set_error('file', t('Failed to move the uploaded file into the %folder folder.',
        ['%folder' => $gpx_dir]));
    }
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }

  $form_state['values']['file_hidden'] = $temp_uri;
}

/**
 * Submit callback.
 */
function gpxtokml_admin_form_transform($form, &$form_state) {
  $file_uri = $form_state['values']['file_hidden'];
  $files = file_load_multiple([], ['uri' => $file_uri]);
  $file = reset($files);

  if (is_object($file)) {
    file_delete($file);
  }
  
  $operation = !empty($form_state['triggering_element']['#attributes']['data-id']) ? 
    $form_state['triggering_element']['#attributes']['data-id'] : '';

  $file_uri = $form_state['values']['file'];
  $icons_set = $form_state['values'][$operation]['icons_set'];

  if ($operation == 'gpx_to_kml') {  
	  // Parse and convert GPX to KML at this point.
	  // @see https://developers.google.com/kml/articles/phpmysqlkml#outputkml
	  $gpx = simplexml_load_file($file_uri);
	  $filename = date('Y.m.d', time()) . '_waypoints';
	  // @todo parse GPX file date and add this date to KML file.
	  // @todo ideally save directly to Google Drive and Shared folder.
	  $file_url = $filename . '-' . $icons_set . '.kml';
	  $visibility = 1;
	  $waypoints_counter = 1;

	  if ($icons_set == 'mapsme') {
		$icons = gpxtokml_get_mapsme_icons();
	  }
	  else {
		$icons = gpxtokml_get_lowrance_icons();
	  }

	  // Creating the basic KML document.
	  $file = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.2"></kml>');
	  $kmlDocument = $file->addChild('Document');

	  // Adding the default styles and icons.
	  foreach ($icons as $icon_id => $icon) {
		$kmlStyle = $kmlDocument->addChild('Style');
		$kmlStyle->addAttribute('id', $icon_id);
		$kmlIconStyle = $kmlStyle->addChild('IconStyle');
		$kmlIcon = $kmlIconStyle->addChild('Icon');
		$kmlIcon->addChild('href', $icon['url']);
	  }

	  // Setting document name and visibility.
	  $kmlDocument->addChild('name', $filename);
	  $kmlDocument->addChild('visibility', $visibility);

	  // Adding waypoints.
	  foreach ($gpx->wpt as $waypoint) {
		$gpx_data = explode(',', (string) $waypoint->sym);
		$gpx_icon_id = $gpx_data[0];
		$gpx_icon_color = !empty($gpx_data[1]) ? $gpx_data[1] : '';

		if ($icons_set == 'mapsme') {
		  $kml_icon_id = gpxtokml_get_lowrance_mapsme_icon_id($waypoint, $gpx_icon_id, $gpx_icon_color);
		}
		else {
		  $kml_icon_id = gpxtokml_get_lowrance_lowrance_icon_id($waypoint, $gpx_icon_id, $gpx_icon_color);
		}

		// @todo use this icon to check unique and new icons.
		$gps_icons_ids[] = $gpx_icon_id;

		$kmlPlacemark = $kmlDocument->addChild('Placemark');

		// @todo make admin configuration.
		if (empty($kml_icon_id) || $icons[$kml_icon_id]['hide_title']) {
		  $kmlPlacemark->addChild('name');
		}
		else {
		  // @todo transliterate.
		  $kmlPlacemark->addChild('name', $waypoint->name);
		}

		// @todo transliterate. Description periodically throws error. Let's skip it for now.
	//    if (!empty($waypoint->desc)) {
	//      $kmlPlacemark->addChild('description', $waypoint->desc);
	//    }
		// @todo define icon mapping.
		$kmlPlacemark->addChild('styleUrl', '#' . $kml_icon_id);
		$kmlPoint = $kmlPlacemark->addChild('Point');
		$kmlPoint->addChild('coordinates', $waypoint['lon'] . ',' . $waypoint['lat']);
		$waypoints_counter++;
	  }	  
  }
  elseif ($operation == 'kml_to_gpx') {
	$filename = date('Y.m.d', time()) . '_waypoints';
	// @todo parse GPX file date and add this date to KML file.
	// @todo ideally save directly to Google Drive and Shared folder.
	$file_url = $filename . '-' . $icons_set . '.gpx';
    $waypoints_counter = 0;
	$result = [];
    $unit = 'Elite-7Ti 107075546';
	
	// 'label', 'icon', 'longitude', 'latitude', 'created'
	
	$file = simplexml_load_file($file_uri);
	foreach ($file->Document->Folder->Placemark as $waypoint) {
		// $name = (string) $waypoint->name;
		$coords = explode(',', (string) $waypoint->Point->coordinates);

		$row = new stdClass;
		$row->label = '';
		$row->longitude = $coords[0];
		$row->latitude = $coords[1];
		$row->icon = 'shortgrass';
		
		$result[] = $row;
	}
	
    // Creating the basic GPX document.
    $file = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="' . $unit . '"></gpx>');

    // Adding waypoints to GPX document.
    foreach ($result as $row) {
      $gpxWpt = $file->addChild('wpt');
      $gpxWpt->addAttribute('lon', $row->longitude);
      $gpxWpt->addAttribute('lat', $row->latitude);
      $gpxWpt->addChild('time', '');
      $gpxWpt->addChild('name', $row->label);
      $gpxWpt->addChild('desc', '');
      $gpxWpt->addChild('sym', $row->icon);

      $waypoints_counter++;
    }	  
  }
  
  if (empty($file) || empty($file_url)) {
	  drupal_set_message(t('File URL is missing.'), 'error');
	  return;
  }

  // Formatting and saving file.
  if (gpxtokml_save_formatted_xml_file($file, $file_url)) {
    drupal_set_message(
      t('File containing !waypoints_counter waypoints has been saved (!file_uri).',
        [
          '!waypoints_counter' => $waypoints_counter,
          '!file_uri' => l($file_url, $file_url, ['attributes' => ['_target' => 'blank']]),
        ]
      )
    );
  }
  else {
    drupal_set_message(t('Error during saving file (!file_uri).', ['!file_uri' => $file_url]), 'error');
  }
}

/**
 * Submit callback.
 */
function gpxtokml_admin_form_export($form, &$form_state) {
  if (!empty($form_state['values']['export_label'])) {
    $label = str_replace(' ', '_', implode('_', $form_state['values']['export_label']));
    $waypoints_counter = 0;
    $gpx_file_url = $label . '.gpx';
    $unit = 'Elite-7Ti 107075546';

    $result = db_select('gpxtokml_chrome_extension_data', 'g')
      ->fields('g', ['label', 'icon', 'longitude', 'latitude', 'created'])
      ->condition('label', $form_state['values']['export_label'], 'IN')
      ->execute()
      ->fetchAll();

    // Creating the basic GPX document.
    $gpx = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="' . $unit . '"></gpx>');

    // Adding waypoints to GPX document.
    foreach ($result as $row) {
      $gpxWpt = $gpx->addChild('wpt');
      $gpxWpt->addAttribute('lon', $row->longitude);
      $gpxWpt->addAttribute('lat', $row->latitude);
      $gpxWpt->addChild('time', $row->created);
      $gpxWpt->addChild('name', $row->label);
      $gpxWpt->addChild('desc', '');
      $gpxWpt->addChild('sym', $row->icon);

      $waypoints_counter++;
    }

    // Formatting and saving GPX file.
    if (gpxtokml_save_formatted_xml_file($gpx, $gpx_file_url)) {
      drupal_set_message(
        t('GPX file containing !waypoints_counter waypoints has been saved (!file_uri).',
          [
            '!waypoints_counter' => $waypoints_counter,
            '!file_uri' => l($gpx_file_url, $gpx_file_url, ['attributes' => ['_target' => 'blank']]),
          ]
        )
      );
    }
    else {
      drupal_set_message(t('Error during saving GPX file (!file_uri).'), 'error');
    }
  }
  else {
    // @todo move this to validation level.
    drupal_set_message('No waipoint label selected', 'error');
  }
}

/**
 * Returns array of unique labels as options.
 *
 * @return array
 */
function gpxtokml_admin_form_transform_get_options() {
  $options = [];
  $result = db_query("SELECT DISTINCT(label) FROM gpxtokml_chrome_extension_data")->fetchAll();

  foreach ($result as $row) {
    $options[$row->label] = $row->label;
  }

  return $options;
}

/**
 * Returns array of Lowrance icons.
 *
 * @return array
 */
function gpxtokml_get_lowrance_icons() {
  $icons = [];
  $icons_ids = [
    'mountains' => [
      'hide_title' => TRUE,
    ],
    'rocks' => [
      'hide_title' => TRUE,
    ],
    'longgrass' => [
      'hide_title' => TRUE,
    ],
    'shortgrass' => [
      'hide_title' => TRUE,
    ],
    'picnictable' => [
      'hide_title' => TRUE,
    ],
    'bigfish' => [
      'hide_title' => FALSE,
    ],
    'smallfish' => [
      'hide_title' => FALSE,
    ],
    'schoolfish' => [
      'hide_title' => FALSE,
    ],
    'stopsign' => [
      'hide_title' => TRUE,
    ],
    'tree' => [
      'hide_title' => FALSE,
    ],
    'exclamation' => [
      'hide_title' => FALSE,
    ],
    'anchor' => [
      'hide_title' => FALSE,
    ],
    'circle' => [
      'hide_title' => FALSE,
    ],
    'circle-blue' => [
      'hide_title' => FALSE,
    ],	
    'circle-magenta' => [
      'hide_title' => FALSE,
    ],		
    'circle-yellow' => [
      'hide_title' => FALSE,
    ],	
    'circle-green' => [
      'hide_title' => FALSE,
    ],		
    'circle-white' => [
      'hide_title' => FALSE,
    ],		
    'circle-cyan' => [
      'hide_title' => FALSE,
    ],		
    'globallocation' => [
      'hide_title' => FALSE,
    ],
    // @todo add icon images to the waypoints below, diamond, red is the default GE icon.
    'diamond' => [
      'hide_title' => FALSE,
    ],
    'l_cross' => [
      'hide_title' => FALSE,
    ],
    // @todo: dangerous zone!!! - add an icon.
    'skullandcrossbones' => [
      'hide_title' => TRUE,
    ],
  ];

  foreach ($icons_ids as $icon_id => $icon_parameters) {
    $icons[$icon_id]['hide_title'] = $icon_parameters['hide_title'];
    // @todo move icons to the better location instead of github.
    $icons[$icon_id]['url'] = GPXTOKML_ICON_BASE_PATH . $icon_id . '.png';
  }

  $additional_icons = [
    'placemark-purple' => [
      'hide_title' => FALSE,
      'url' => 'http://mapswith.me/placemarks/placemark-purple.png',
    ],
    'placemark-blue' => [
      'hide_title' => FALSE,
      'url' => 'http://mapswith.me/placemarks/placemark-blue.png',
    ],
  ];

  return ($icons + $additional_icons);
}

/**
 * Returns array of MAPS.ME icons.
 *
 * @return array
 */
function gpxtokml_get_mapsme_icons() {
  $icons = [];
  $icons_ids = [
    'placemark-yellow' => [
      'hide_title' => TRUE,
    ],
    'placemark-green' => [
      'hide_title' => TRUE,
    ],
    'placemark-red' => [
      'hide_title' => FALSE,
    ],
    'placemark-pink' => [
      'hide_title' => FALSE,
    ],
    'placemark-orange' => [
      'hide_title' => FALSE,
    ],
    'placemark-purple' => [
      'hide_title' => FALSE,
    ],
    'placemark-brown' => [
      'hide_title' => FALSE,
    ],
    'placemark-blue' => [
      'hide_title' => FALSE,
    ],
  ];

  foreach ($icons_ids as $icon_id => $icon_parameters) {
    $icons[$icon_id]['hide_title'] = $icon_parameters['hide_title'];
    $icons[$icon_id]['url'] = 'http://mapswith.me/placemarks/' . $icon_id . '.png';
  }

  return $icons;
}

/**
 * Returns MAPS.ME equivalent of Lowrance icon.
 *
 * @param $waypoint
 * @param $lowrance_icon_id
 * @param $gpx_icon_color
 *
 * @return string
 */
function gpxtokml_get_lowrance_mapsme_icon_id($waypoint, $lowrance_icon_id, $gpx_icon_color = '') {
  // Define icon mapping.
  switch ($lowrance_icon_id) {
    // Brovka.
    case 'mountains':
    case 'rocks':
      $mapsme_icon_id = 'placemark-yellow';
      break;
    // Koryagi raznyh vidov.
    case 'longgrass':
    case 'shortgrass':
    case 'picnictable':
    // @todo: think about dangerous zone.
    case 'skullandcrossbones':
      $mapsme_icon_id = 'placemark-green';
      break;
    // Sudak.
    case 'bigfish':
      $mapsme_icon_id = 'placemark-red';
      break;
    // Shchuka.
    case 'smallfish':
      $mapsme_icon_id = 'placemark-pink';
      break;
    // Okun.
    case 'schoolfish':
      $mapsme_icon_id = 'placemark-orange';
      break;
    case 'stopsign':
    case 'tree':
    case 'globallocation':
    case 'exclamation':
    case 'l_cross':
      $mapsme_icon_id = 'placemark-purple';
      break;
    case 'anchor':
      $mapsme_icon_id = 'placemark-brown';
      break;
    // Glubina.
    case 'circle':
	  if ($gpx_icon_color == 'magenta') {
        $mapsme_icon_id = 'placemark-purple';	  
	  }
	  elseif ($gpx_icon_color == 'green') {
        $mapsme_icon_id = 'placemark-green';
      }	  
	  elseif ($gpx_icon_color == 'yellow') {
        $mapsme_icon_id = 'placemark-yellow';
      }		
	  elseif ($gpx_icon_color == 'red') {
        $mapsme_icon_id = 'placemark-red';
      }		  
      else {
	    // Default, cyan and blue colors.
		$mapsme_icon_id = 'placemark-blue';
	  }	  
      break;	
    case 'diamond';
      $mapsme_icon_id = 'placemark-blue';
      break;
    default:
      $mapsme_icon_id = 'placemark-blue';
  }

  // Brovka.
  if (in_array($waypoint->name, ['B', 'b'])) {
    $mapsme_icon_id = 'placemark-yellow';
  }
  // Korchi.
  elseif (in_array($waypoint->name, ['K', 'k'])) {
    $mapsme_icon_id = 'placemark-green';
  }

  return $mapsme_icon_id;
}

/**
 * Returns Lowrance remote equivalent (Google Earth) of Lowrance built-in icon.
 *
 * @param $waypoint
 * @param $gpx_icon
 * @param $gpx_icon_color
 *
 * @return string
 */
function gpxtokml_get_lowrance_lowrance_icon_id($waypoint, $gpx_icon, $gpx_icon_color = '') {
  switch ($gpx_icon) {
    case 'circle':
      if (!empty($gpx_icon_color) && in_array($gpx_icon_color, gpxtokml_get_lowrance_icon_colors())) {
        $kml_icon_id = 'circle' . '-' . $gpx_icon_color;
      }
      else {
        $kml_icon_id = 'circle';
      }
      break;
    // diamond, red is the default GE icon.
    case 'diamond':
      // Brovka.
      if (in_array($waypoint->name, ['B', 'b'])) {
        $kml_icon_id = 'mountains';
      }
      // Korchi.
      elseif (in_array($waypoint->name, ['K', 'k'])) {
        $kml_icon_id = 'shortgrass';
      }
      else {
        $kml_icon_id = 'placemark-blue';
      }
      break;
    default:
      $kml_icon_id = $gpx_icon;
  }

  return $kml_icon_id;
}

/**
 * Returns list of colors used for some Lowrance icons (such as circle).
 *
 * @return array
 */
function gpxtokml_get_lowrance_icon_colors() {
  // Valid only for circle.
  return [
    'blue',
    'magenta',
    'yellow',
    'white',
    'red',
    'green',
    'cyan',
  ];
}

/**
 * Saves formatted XML file.
 *
 * @param \SimpleXMLElement $kml
 * @param $file_url
 *
 * @return int
 */
function gpxtokml_save_formatted_xml_file(SimpleXMLElement $xml, $file_url) {
  $dom = new DOMDocument('1.0');
  $dom->preserveWhiteSpace = FALSE;
  $dom->formatOutput = TRUE;
  $dom->loadXML($xml->asXML());
  echo $dom->saveXML();

  return $dom->save($file_url);
}
